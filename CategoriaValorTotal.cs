    public class CategoriaValorTotal
    {
        required public int Categoria { get; set; }
        public double ValorTotal { get; set; }
    }