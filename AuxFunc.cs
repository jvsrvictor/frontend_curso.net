using System.Text.RegularExpressions;

// FUNÇÕES AUXILIARES --------------------------------------------------------------
namespace MudBlazor
{
    public static class AuxFunc
    {

    // VALIDAR EMAIL
    public static bool ValidarEmail(string emailAddress)
    {
        var pattern = @"^[a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$";  
        var regex = new Regex(pattern);
        return regex.IsMatch(emailAddress);
    }

    // VERIFICA SE STRING TEM CARACTERES ESPECIAIS
    public static bool HasSpecialChars(string yourString)
    {
        return yourString.Any(ch => ! char.IsLetterOrDigit(ch));
    }

    // FORMATA O VALOR DOUBLE PARA FORMATO MONETÁRIO PT-BR
    public static string Valor(double Valor)
    {
        return Valor.ToString("C");
    }

    // APRESENTAÇÃO DE DADOS --------------------------------------------------------------
    
    // RETORNA COM O NOME DA CONTA DADO O ID DELA
    public static string NomeConta(int IdConta)
    {   
        switch (IdConta){

            case 0:
                return "Conta Corrente";

            case 1:
                return "Conta Poupança";

            case 2:
                return "Carteira";

            default:
                return "Conta Inexistente";
        }
    }

    // RETORNA O ÍCONE (MUD ICON) DA CONTA DADO O ID DELA
    public static string IconeConta(int IdConta)
    {   
        switch (IdConta){

            case 0:
                return MudBlazor.Icons.Material.Filled.AccountBalanceWallet;

            case 1:
                return MudBlazor.Icons.Material.Filled.Savings;

            case 2:
                return MudBlazor.Icons.Material.Filled.Wallet;

            default:
                return MudBlazor.Icons.Material.Filled.Block;
        }
    }

    // RETORNA UMA COR ESPECÍFICA PARA O TIPO DE TRANSAÇÃO (RECEITA | DESPESA)
    public static MudBlazor.Color CorTransacao(bool TipoTransacao)
    {
        if (TipoTransacao)
        {
            return MudBlazor.Color.Success;
        }
        return MudBlazor.Color.Error;
    }
    
    // RETORNA UMA COR (STRING CSS) ESPECÍFICA PARA O TIPO DE TRANSAÇÃO (RECEITA | DESPESA)
    public static string CorTransacaoString(bool TipoTransacao)
    {
        if (TipoTransacao)
        {
            return "var(--mud-palette-success)";
        }
        return "var(--mud-palette-error)";
    }

    // RETORNA UM ÍCONE ESPECÍFICO DADO O ID DA CATEGORIA DA TRANSAÇÃO
    public static string IconeCategoria(int IdCategoria)
    {
        switch(IdCategoria) 
        {
        case 0:
            // Salário
            return Icons.Material.Filled.LocalAtm;
        case 1:
            // Benefícios
            return Icons.Material.Filled.LocalAtm;
        case 2:
            // Saúde
            return Icons.Material.Filled.LocalHospital;
        case 3:
            // Alimentação
            return Icons.Material.Filled.LocalDining;
        case 4:
            // Compras
            return Icons.Material.Filled.ShoppingBag;
        case 5:
            // Outros
            return Icons.Material.Filled.Spoke;
        default:
            return "???";
        }
    }

    // FORMATA O CAMPO 'CATEGORIA', ALTERA O TEXTO DE ACORDO COM O CÓDIGO
    public static string CategoriaTexto(int IdCategoria)
    {
        switch(IdCategoria) 
        {
        case 0:
            // Salário
            return "Salário";
        case 1:
            // Benefícios
            return "Benefícios";
        case 2:
            // Saúde
            return "Saúde";
        case 3:
            // Alimentação
            return "Alimentação";
        case 4:
            // Compras
            return "Compras";
        case 5:
            // Outros
            return "Outros";
        default:
            return "???";
        }
    }
    
    // FORMATA O CAMPO 'CATEGORIA', ADICIONA O ÍCONE DE ACORDO COM O CÓDIGO
    public static string CategoriaIcone(int IdCategoria)
    {
        switch(IdCategoria) 
        {
        case 0:
            // Salário
            return @Icons.Material.Filled.LocalAtm;
        case 1:
            // Benefícios
            return @Icons.Material.Filled.LocalAtm;
        case 2:
            // Saúde
            return @Icons.Material.Filled.LocalHospital;
        case 3:
            // Alimentação
            return @Icons.Material.Filled.LocalDining;
        case 4:
            // Compras
            return @Icons.Material.Filled.ShoppingBag;
        case 5:
            // Outros
            return @Icons.Material.Filled.Spoke;
        default:
            return "???";
        }
    }

    public static string CategoriaCorString(int IdCategoria)
    {
        switch(IdCategoria) 
        {
        case 0:
            // Salário
            return "#3949AB";
        case 1:
            // Benefícios
            return "#00897B";
        case 2:
            // Saúde
            return "#B71C1C";
        case 3:
            // Alimentação
            return "#558B2F";
        case 4:
            // Compras
            return "#D81B60";
        case 5:
            // Outros
            return "#455A64";
        default:
            return "???";
        }
    }
        
    // FORMATA O CAMPO 'RECORRÊNCIA' DE ACORDO COM O CÓDIGO
    public static string Recorrencia(int Recorrencia, int Recorrencia_Modo, int Recorrencia_N_Vezes, DateTime? Recorrencia_DataLimite)
    {   
        string textoRetorno1 = "";
        string textoRetorno2 = "";


        switch(Recorrencia) 
        {
            case 0:
            textoRetorno1 = "Nenhuma";
            return textoRetorno1;
            break;

            case 1:
            textoRetorno1 = "Diária";
            break;

            case 2:
            textoRetorno1 = "Semanal";
            break;

            case 3:
            textoRetorno1 = "Mensal";
            break;

            case 4:
            textoRetorno1 = "Anual";
            break;

            default:
            textoRetorno1 = "???";
            break;
        }


        switch(Recorrencia_Modo) 
        {
            case 0:
            textoRetorno2 = ", indefinidamente";
            break;

            case 1:
            textoRetorno2 = ", até o dia " + Recorrencia_DataLimite?.ToString("dd/MM/yyyy");
            break;

            case 2:
            textoRetorno2 = ", " + Recorrencia_N_Vezes + " vezes";
            break;

            default:
            textoRetorno2 = "???";
            break;
        }


        return textoRetorno1 + textoRetorno2;
    }

    public static bool VerificarDiferencaInteiraDeMeses(DateTime? data1, DateTime? data2)
    {
        // Verifica se alguma das datas é nula
        if (!data1.HasValue || !data2.HasValue)
            return false;

        // Remove o horário das datas, mantendo apenas a data
        DateTime data1SemHorario = data1.Value.Date;
        DateTime data2SemHorario = data2.Value.Date;

        // Calcula a diferença entre os anos e os meses
        int anosDeDiferenca = data2SemHorario.Year - data1SemHorario.Year;
        int mesesDeDiferenca = (data2SemHorario.Month + anosDeDiferenca * 12) - data1SemHorario.Month;

        // Verifica se a diferença é de um número inteiro de meses e se os dias são iguais
        return data1SemHorario.AddMonths(mesesDeDiferenca) == data2SemHorario && data1SemHorario.Day == data2SemHorario.Day;
    }

    public static bool VerificarDiferencaInteiraDeAnos(DateTime? data1, DateTime? data2)
    {
        // Verifica se alguma das datas é nula
        if (!data1.HasValue || !data2.HasValue)
            return false;

        // Remove o horário das datas, mantendo apenas a data
        DateTime data1SemHorario = data1.Value.Date;
        DateTime data2SemHorario = data2.Value.Date;

        // Calcula a diferença entre os anos
        int anosDeDiferenca = data2SemHorario.Year - data1SemHorario.Year;

        // Verifica se a diferença é de um número inteiro de anos e se os dias e meses são iguais
        return data1SemHorario.AddYears(anosDeDiferenca) == data2SemHorario && 
               data1SemHorario.Day == data2SemHorario.Day &&
               data1SemHorario.Month == data2SemHorario.Month;
    }

    public static int? CalcularMesesEntreDatas(DateTime? dataInicial, DateTime? dataFinal)
    {
        // Verifica se alguma das datas é nula
        if (!dataInicial.HasValue || !dataFinal.HasValue)
            return null;

        // Calcula a diferença total de meses entre as datas
        int meses = (dataFinal.Value.Year - dataInicial.Value.Year) * 12 + dataFinal.Value.Month - dataInicial.Value.Month;

        // Ajusta a contagem se o dia da data final for menor que o dia da data inicial
        if (dataFinal.Value.Day < dataInicial.Value.Day)
        {
            meses--;
        }

        return meses;
    }

    public static int? CalcularAnosEntreDatas(DateTime? dataInicial, DateTime? dataFinal)
    {
        // Verifica se alguma das datas é nula
        if (!dataInicial.HasValue || !dataFinal.HasValue)
            return null;

        // Calcula a diferença de anos
        int anos = dataFinal.Value.Year - dataInicial.Value.Year;

        // Ajusta a contagem se a data final ainda não passou o mês e dia da data inicial
        if (dataFinal.Value.Month < dataInicial.Value.Month ||
           (dataFinal.Value.Month == dataInicial.Value.Month && dataFinal.Value.Day < dataInicial.Value.Day))
        {
            anos--;
        }

        return anos;
    }

    // RECEBE UMA DATA, DADOS DE RECORRÊNCIA, E RETORNA SE A DATA É COMPATÍVEL OU NÃO
    public static bool VerificaDataCompativel(DateTime? DataHoje, DateTime? DataTransacao, int Frequencia, DateTime? DataLimite, int Modo, int N_Vezes)
    {
        // INDEFINIDAMENTE
        if(Modo==1)
        {   

            TimeSpan? diferenca = DataTransacao?.Date - DataHoje?.Date;

            // VERIFICA SE A FREQUENCIA (DIARIA | SEMANAL | MENSAL | ANUAL) É COMPATÍVEL | SE A DataHoje É MULTIPLO DE DataTransacao
            switch(Frequencia)
            {   

                // SE FOR DIÁRIA
                case 1: return diferenca?.TotalDays >= 1 ? true : false;                    

                // SE FOR SEMANAL
                case 2: return diferenca?.TotalDays % 7 == 0 ? true : false;  

                // SE FOR MENSAL
                case 3: return AuxFunc.VerificarDiferencaInteiraDeMeses(DataTransacao, DataHoje);

                // SE FOR ANUAL
                case 4: return AuxFunc.VerificarDiferencaInteiraDeAnos(DataTransacao, DataHoje);

                default: return false;

            }
        }

        // ATÉ O DIA
        if(Modo==2)
        {   
            // SE AINDA ESTÁ DENTRO DO LIMITE
            if(DataHoje <= DataLimite)
            {
                TimeSpan? diferenca = DataTransacao?.Date - DataHoje?.Date;

                // VERIFICA SE A FREQUENCIA (DIARIA | SEMANAL | MENSAL | ANUAL) É COMPATÍVEL | SE A DataHoje É MULTIPLO DE DataTransacao
                switch(Frequencia)
                {   

                    // SE FOR DIÁRIA
                    case 1: return diferenca?.TotalDays >= 1 ? true : false;                    

                    // SE FOR SEMANAL
                    case 2: return diferenca?.TotalDays % 7 == 0 ? true : false;  

                    // SE FOR MENSAL
                    case 3: return AuxFunc.VerificarDiferencaInteiraDeMeses(DataTransacao, DataHoje);

                    // SE FOR ANUAL
                    case 4: return AuxFunc.VerificarDiferencaInteiraDeAnos(DataTransacao, DataHoje);

                    default: return false;

                }

            }
            else
            {
                return false;
            }
        }

        // NÚMERO DE VEZES
        if(Modo==3)
        {
            TimeSpan? diferenca = DataTransacao?.Date - DataHoje?.Date;

            switch(Frequencia)
            {   

                // SE FOR DIÁRIA
                case 1: return diferenca?.TotalDays <= N_Vezes ? true : false;                    

                // SE FOR SEMANAL
                case 2: return (diferenca?.TotalDays % 7 == 0) && (diferenca?.TotalDays/7 <= N_Vezes) ? true : false;  

                // SE FOR MENSAL
                case 3: return AuxFunc.VerificarDiferencaInteiraDeMeses(DataTransacao, DataHoje) && (AuxFunc.CalcularMesesEntreDatas(DataTransacao, DataHoje) <= N_Vezes) ? true : false;

                // SE FOR ANUAL
                case 4: return AuxFunc.VerificarDiferencaInteiraDeAnos(DataTransacao, DataHoje) && (AuxFunc.CalcularAnosEntreDatas(DataTransacao, DataHoje) <= N_Vezes) ? true : false;

                default: return false;

            }
        }

        return true;
    }






    }
}