// FUNÇÕES AUXILIARES --------------------------------------------------------------
namespace MudBlazor
{
    public class SaldoDias
    {
        required public string Dia { get; set; }
        public double Saldo { get; set; }
    }
}